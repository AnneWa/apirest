package com.SpringBootIII.apiRest.controller;

import com.SpringBootIII.apiRest.model.Contact;
import com.SpringBootIII.apiRest.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
@RequestMapping("contacts")
@RestController
public class ContactController {

    @Autowired
    private ContactService contactService;


    @GetMapping()
    public Iterable<Contact> getContacts(){
        return contactService.getContacts();
    }

    @GetMapping("/{id}")
    public Contact getContactById(@PathVariable("id") final Long id){
        Optional<Contact> contact = contactService.getContact(id);
        if(contact.isPresent()) {
            return contact.get();
        } else {
            return null ;
        }
    }

    @PutMapping("/{id}")
    public Contact updateContact(@PathVariable("id") final Long id, @RequestBody Contact contact) {
        Optional<Contact> c = contactService.getContact(id);
        if(c.isPresent()) {
            Contact currentContact = c.get();

            String firstName = contact.getFirstName();
            if(firstName != null) {
                currentContact.setFirstName(firstName);
            }
            String lastName = contact.getLastName();
            if(lastName != null) {
                currentContact.setLastName(lastName);
            }
            int age = contact.getAge();
            if(age != 0) {
                currentContact.setAge(age);
            }

            contactService.saveContact(currentContact);
            return currentContact;
        } else {
            return null;
        }
    }

    @DeleteMapping("/{id}")
    public String deleteEmployee(@PathVariable("id") final Long id) {
        contactService.deleteContact(id);
            return "L'employé a été supprimé";
        }


    @PostMapping()
    public Contact createContact(@RequestBody Contact contact){
        return contactService.saveContact(contact);
    }

}
