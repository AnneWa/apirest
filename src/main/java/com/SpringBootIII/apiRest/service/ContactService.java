package com.SpringBootIII.apiRest.service;

import com.SpringBootIII.apiRest.model.Contact;
import com.SpringBootIII.apiRest.repository.ContactRepository;
import jdk.jfr.DataAmount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.Optional;


@Service
public class ContactService {

    @Autowired

    private ContactRepository contactRepository;

    public Optional<Contact> getContact(final Long id){
        return contactRepository.findById(id);
    }

    public Iterable<Contact> getContacts(){
        return contactRepository.findAll();
    }

    public void deleteContact(final Long id){
        contactRepository.deleteById(id);
    }

    public Contact saveContact(Contact contact){
        Contact savedContact = contactRepository.save(contact);
        return savedContact;
    }

}
